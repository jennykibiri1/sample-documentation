# README FLUX CAPACITOR UI

## Pre-requisites

- _Node:_ `^16.13.0` or higher.
- _Npm:_ `8.1.0` or higher.

## Getting started

Clone the project from Bitbucket: :

```sh
git clone git@github.com:...

cd thh-intacct-ui
```

Install dependencies :

```sh
npm install
```

Setup development environment:

```sh
npm run start:timesheet
```

That's it you're ready to start contributing 👍!

## Contribution guidelines

- Development and testing is done in the `thh-intacct-ui` develop branch: `git checkout develop`

  - s3 bucket link for devlopment and testing: ``

- Production is done in the `thh-intacct-ui` master branch: `git checkout master`

  - s3 bucket link for production:``

- Commit message: `[<type>] <subject>`

  - type: `feat`, `fix`, `docs`, `style`, `refactor`, `test`
  - subject: `<subject>`
  - example: `feat: add new feature`

- Only merge to master if the ticket is closed

## Framework, library and Ui Kits used

- React: `^17.0.39`
- TypeScript: `^^4.5.5`
- JQuery: `^3.6.0"`
- metronic - `https://preview.keenthemes.com/metronic/demo9/index.html`

## Project structure

src has 4 project folders:

### 1. Configuration - contains files for the configuration ui
  - add/edit/delete/list/view - Organizations
  - link:`http://fc-config.s3-website-us-east-1.amazonaws.com/`
### 2. Projects - contains files for jira project mapping
  - maps jira project to intacct project
  - link:`http://fc-project.s3-website-us-east-1.amazonaws.com/`
### 3. Resources - contains files for the resource ui
  - maps a jira user to intacct user
  - link:`http://fc-resource.s3-website-us-east-1.amazonaws.com/`
### 4. Timesheet contains files for the timesheet ui

  - imports and exports timesheet data
  - list resource without time
  - fill missing time for resources
  - list unmapped resources and projects
  - list resource without billing
  - link: `http://fc-timesheet.s3-website-us-east-1.amazonaws.com/`

## Components
### src/timesheet has the following components:

- ImportTimesheet - component for importing jira timesheets
- ResourceMissingTime - component for displaying resource missing time
- ResourceMissingMapping - component for displaying resource missing mapping
- ProjectMissingMap - component for displaying project missing mapping
- ProjectResourceWithoutBilling - component for displaying project resource without billing
- ExportTimesheet - component for exporting timesheet to intacct
- Wizard - component for wizard ui

### src/projects has the following components:

- modal form for mapping jira project to intacct project
- Table for displaying jira projects

### src/resource has the following components:

- modal form for mapping jira user to intacct user
- Table for displaying jira users

### src/configuration has the following components:

- model form for adding Organizations
- Table for displaying Organizations

## Intacct API and Lambda functions

- intacct
- api credentials: utils/intacct.ts

- api-call folders:
  All apis calls are in the api/service folder

Refer to the [Intacct API](https://developer.intacct.com/api/project-resource-mgmt/) for more information.

### Who do I talk to?

- Repo owner or admin:
  - Aaron CTO: `aaron.ertler@thejitu.com`
  - Rodgers Developer: `rodgers.wanyonyi@thejitu.com`
  - Jeniffer Developer: `jennykibiri@thejitu.com`
  - Kevin QA/BA: `kelvin.macharia@thejitu.com`
  - Jessica QA/PM: `jessica.eberhart@thejitu.com`
